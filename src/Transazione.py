# CLASSE TRANSAZIONE
class Transazione:
    
    def __init__(self, sender: str = None, receiver: str = None, atm: str = None) -> None:
        import random
        from functions import gen_datetime
        
        self.sender = sender
        self.receiver = receiver
        self.atm = atm
        self.amount = random.randint(10, 3000) # LE TRANSAZIONI NON POSSONO SUPERARE 3.000 EURO
        self.timestamp = gen_datetime()

    def dictify(self):
        data={}
        data['sender'] = self.sender
        data['receiver'] = self.receiver
        data['atm'] = self.atm
        data['amount'] = self.amount
        data['timestamp'] = self.timestamp
        return data

