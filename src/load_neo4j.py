# MAIN LOAD DATA NEO4J

from Neo4jController import Neo4jController

neo = Neo4jController(4)

neo.reset()
neo.load_data()

print("DONE!")
