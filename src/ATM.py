#CLASSE ATM
class ATM:
    def __init__(self) -> None:
        from faker import Faker

        fk = Faker('it_IT')
        self.address = (fk.address()).replace('\n',' ')[:-5]
        
    def dictify(self):
        data={}
        data['address'] = self.address
        return data
