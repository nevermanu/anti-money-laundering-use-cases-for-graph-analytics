
class MongoController:

    def __init__(self, level: int) -> None:
        from pymongo import MongoClient

        client = MongoClient("localhost", 27017)
        
        self.db = client['anti-fraud']
        self.level = level

    def load_carte(self):
        import csv

        with open(f'carte{self.level}.csv', 'r') as csvfile:
            header = [ "code", "owner","bank","cash"]
            reader = csv.reader(csvfile)

            header_row = True
            for row in reader:
                if header_row:
                    header_row = False
                    continue
                doc={}
                for n in range(0,len(header)):
                    doc[header[n]] = row[n]

                self.db.Carta.insert_one(doc)

    def load_persone(self):
        import csv

        with open(f'persone{self.level}.csv', 'r') as csvfile:
            header = [ "name","surname","sex","dob","address","city","phone_number","cf"]
            reader = csv.reader(csvfile)

            header_row = True
            for row in reader:
                if header_row:
                    header_row = False
                    continue
                doc={}
                for n in range(0,len(header)):
                    doc[header[n]] = row[n]

                self.db.Persona.insert_one(doc)

    def load_banche(self):
        import csv

        with open(f'banche{self.level}.csv', 'r') as csvfile:
            header = ["name","manager"]
            reader = csv.reader(csvfile)

            header_row = True
            for row in reader:
                if header_row:
                    header_row = False
                    continue
                doc={}
                for n in range(0,len(header)):
                    doc[header[n]] = row[n]

                self.db.Banca.insert_one(doc)

    def load_atms(self):
        import csv

        with open(f'atms{self.level}.csv', 'r') as csvfile:
            header = ["address"]
            reader = csv.reader(csvfile)

            header_row = True
            for row in reader:
                if header_row:
                    header_row = False
                    continue
                doc={}
                for n in range(0,len(header)):
                    doc[header[n]] = row[n]

                self.db.ATM.insert_one(doc)   

    def load_transazioni(self):
        import csv

        with open(f'transazioni{self.level}.csv', 'r') as csvfile:
            header = ["sender","receiver","atm","amount","timestamp"]
            reader = csv.reader(csvfile)

            header_row = True
            for row in reader:
                if header_row:
                    header_row = False
                    continue
                doc={}
                for n in range(0,len(header)):
                    doc[header[n]] = row[n]

                self.db.Transazione.insert_one(doc)

    def create_relations(self):
        import csv

        with open(f'bank_relations{self.level}.csv', 'r') as csvfile:
            reader = csv.reader(csvfile)

            relations = {}
            header_row = True
            for row in reader:
                if header_row:
                    header_row = False
                    continue
                try:
                    relations[row[0]] += [row[1]]
                except:
                    relations[row[0]] = [row[1]]
            
        for key in relations:
            self.db.Banca.update_one({'name':key}, {'$set':{'relations':relations[key]}}, upsert = False)

    def load_data(self):

        self.load_persone()
        self.load_atms()
        self.load_banche()
        self.load_carte()
        self.load_transazioni()
        self.create_relations()

    def query1(self):
        # TUTTE LE TRANSAZIONI CON AMOUNT > 1000
        transactions = self.db.Transazione.find({'$where':'this.amount > 1000'})
        transactions_list = []
        for transaction in transactions:
            transactions_list.append(transaction)
        return transactions_list
        
    
    def query2(self):
        # TROVA LE PERSONE CHE POSSEGGONO UNA CARTA DI UNA DETERMINATA BANCA
        bank_name = ['Jarvis', 'Mitchell', 'Daniels', 'Clark'][self.level-1]
        cards = self.db.Carta.find({'bank': bank_name})
        cf_owners = []
        for card in cards:
            cf_owners.append(card['owner'])
        persons = []
        persons += list(self.db.Persona.find({'cf':{'$in':cf_owners}}))
        return persons
    
    def query3(self):
        # TROVA LE PERSONE CHE POSSEGGONO UNA CARTA DELLE BANCHE CON CUI HA RAPPORTI UNA DETERMINATA BANCA
        bank_name = ['Jarvis', 'Mitchell', 'Daniels', 'Clark'][self.level-1]
        bank = self.db.Banca.find({'name':bank_name})[0]
        relations = bank['relations']
        cards = []
        for rel in relations:
            cards += list(self.db.Carta.find({'bank': rel}))
        persons = []
        for card in cards:
            persons += list(self.db.Persona.find({'cf': card['owner']}))
        return persons

    def query4(self):
        # TROVA GLI ATM IN CUI SONO AVVENUTE DELLE TRANSAZIONI SUPERIORI A 1000 
        # CON MITTENTE UNA CARTA DI UNA DETERMINATA BANCA
        bank_name = ['Jarvis', 'Mitchell', 'Daniels', 'Clark'][self.level-1]
        cards = self.db.Carta.find({'bank':bank_name})
        cards = [card['code'] for card in cards]
        transactions =  list(self.db.Transazione.find({'sender':{'$in':cards}, '$where':'this.amount > 1000'}))
        atms = []
        for transaction in transactions:
            atms.append(transaction['atm'])
        atms_list = []
        atms_list.append(list(self.db.ATM.find({'address':{'$in':atms}})))
        return atms_list

    def query5(self):
        # TROVA LE PERSONE CHE HANNO CARTE CON PIU' DI 800.000 DI CASH CHE HANNO ESEGUITO TRANSAZIONI IN USCITA (SENDER)
        # DI PIU' DI 2000 DI AMOUNT CON ALTRE CARTE CON MENO DI 100.000 DI CASH
        cards = self.db.Carta.find({'$where':'this.cash < 100000'})
        transactions = []
        cards_codes = [card['code'] for card in cards]
        transactions += list(self.db.Transazione.find({'receiver':{'$in':cards_codes}, '$where':'this.amount > 2000'}))
        cards = []
        for transaction in transactions:
            sender = transaction['sender']
            cards += list(self.db.Carta.find({'code':sender, '$where':'this.cash > 800000'}))
        cfs = []
        for card in cards:
            cfs.append(card['owner'])
        persons = []
        persons += list(self.db.Persona.find({'cf':{'$in':cfs}}))
        return persons
        
            
        


    def clear_cache(self):

        self.db.command({"planCacheClear" : "Persona"})
        self.db.command({"planCacheClear" : "Carta"})
        self.db.command({"planCacheClear" : "ATM"})
        self.db.command({"planCacheClear" : "Banca"})
        self.db.command({"planCacheClear" : "Transazione"})
        
    def reset(self):

        self.db.Persona.drop()
        self.db.Carta.drop()
        self.db.Banca.drop()
        self.db.ATM.drop()
        self.db.Transazione.drop()
        self.clear_cache()

    
