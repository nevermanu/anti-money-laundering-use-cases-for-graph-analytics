import functions
from Neo4jController import Neo4jController 
from MongoController import MongoController
import winsound
import xlsxwriter

ds = 4 #

neo = Neo4jController(ds)
mongo = MongoController(ds)
neo.reset()
mongo.reset()
neo.load_data()
print("DS caricato su Neo4j")
mongo.load_data()
print("DS caricato su mongoDB")
workbook = xlsxwriter.Workbook(f'query of dataset {ds}.xlsx') ####
functions.time_trial(workbook, neoInstance=neo, mongoInstance=mongo, dataset_number=ds) ####
workbook.close()
print(f"Fine DS {ds}")
winsound.MessageBeep()
winsound.MessageBeep()