# CLASSE CARTA
class Carta:
    def __init__(self, owner: str = None, bank: str = None) -> None:
        import random

        code = ''
        for _ in range(16):
            code += str(random.randint(0,9))
        
        self.code = code
        self.owner = owner
        self.bank = bank
        self.cash = random.randint(10, 1_000_000)

    def set_owner(self, cf: str):
        self.owner = cf

    def set_bank(self, name: str):
        self.bank = name
    
    def dictify(self):
        data = {}
        data['code'] = self.code
        data['owner'] = self.owner
        data['bank'] = self.bank
        data['cash'] = self.cash
        return data