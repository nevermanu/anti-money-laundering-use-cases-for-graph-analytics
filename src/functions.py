# FUNZIONI EXTRA
def gen_datetime(min_year=2021, max_year=2021):
    import random
    from datetime import datetime, timedelta
    start = datetime(min_year, 1, 1, 00, 00, 00)
    years = max_year - min_year + 1
    end = start + timedelta(days=365 * years)
    return start + (end - start) * random.random()

def write_csv(dict_list: list, filename: str = 'output'):
    import csv
    toCSV = dict_list
    keys = toCSV[0].keys()
    with open(f'{filename}.csv', 'w', newline='')  as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(toCSV)

def list_of_dictify(lista: list) -> list:
    result = []
    for obj in lista:
        result.append(obj.dictify())
    return result

def time_trial(workbook, neoInstance, mongoInstance, dataset_number: int):
    import time
    tempo = lambda: int(round(time.time() * 1000))
    
    worksheet = workbook.get_worksheet_by_name('Sheet1')
    if worksheet == None:
        worksheet = workbook.add_worksheet()
    row = 1
    col = 0

    mongo_queries = [mongoInstance.query1, mongoInstance.query2, mongoInstance.query3, mongoInstance.query4, mongoInstance.query5]
    neo_queries = [neoInstance.query1, neoInstance.query2, neoInstance.query3, neoInstance.query4, neoInstance.query5]
    worksheet.write(0, col, "Neo4j")
    worksheet.write(0, col+1, "MongoDB")

    for query in neo_queries:
        worksheet.write(0, col, "Neo4j")
        for _ in range(31):
            neo1 = tempo()
            query()
            neo2 = tempo()
            worksheet.write(row, col, neo2-neo1)
            row+=1
        row = 1
        col += 2
        print("fine query neo")

    col = 1
    for query in mongo_queries:
        worksheet.write(0, col, "MongoDB")
        for _ in range(31):
            mongo1 = tempo()
            query()
            mongo2 = tempo()
            worksheet.write(row, col, mongo2-mongo1)
            row+=1
        row = 1
        col += 2
        print("fine query mongo")
    





    '''
    for y in range(31):
        neo1 = tempo()
        neo4jquery()
        neo2 = tempo()
        worksheet.write(row, col, neo2-neo1)

        mongo1 = tempo()
        mongoquery()
        mongo2 = tempo()
        worksheet.write(row, col+1, mongo2-mongo1)

        row += 1
    '''   