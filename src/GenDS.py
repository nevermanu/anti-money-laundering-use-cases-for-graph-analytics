# Classe per la generazione del dataset. 
# Le regole per la generazione sono le seguenti:
#
# 1. Una persona può avere da 1 a 5 carte
# 2. Le carte possono appartenere ad un’unica banca
# 3. Una persona viene identificata tramite un codice fiscale
# 4. Una carta viene identificata tramite il suo codice
# 5. Una banca viene identificata tramite il suo nome
# 6. Una transazione viene identificata da un id univoco
# 7. Un ATM viene identificato dalla locazione geografica (via)
# 8. Una transazione può avvenire solo tra due carte diverse appartenenti a due persone diverse
# 9. Una transazione può avvenire in un unico ATM
# 10. Ogni banca può avere accordi con ogni altra banca in maniera casuale
#


class GenDS:
    # N.B. IL LIVELLO E' LA PERCENTUALE DEL DATASET DESIDERATO:
    # LIVELLO 1: DATASET 25%
    # LIVELLO 2: DATASET 50%
    # LIVELLO 3: DATASET 75%
    # LIVELLO 4: DATASET 100%
    def __init__(self, level: int) -> None: 
        import functions

        self.persone = []
        self.carte = []
        self.banche = []
        self.atms = []
        self.transazioni = []
        self.bank_relations = []

        self.div = level/4

        self.gen_persone()
        self.gen_banche()
        self.gen_relazioni_banche()
        self.gen_carte()
        self.gen_atms()
        self.gen_transazioni()

        functions.write_csv(dict_list=functions.list_of_dictify(self.persone) , filename=f'persone{level}')
        functions.write_csv(dict_list=functions.list_of_dictify(self.carte), filename=f'carte{level}')
        functions.write_csv(dict_list=functions.list_of_dictify(self.banche), filename=f'banche{level}')
        functions.write_csv(dict_list=functions.list_of_dictify(self.atms), filename=f'atms{level}')
        functions.write_csv(dict_list=functions.list_of_dictify(self.transazioni), filename=f'transazioni{level}')
        functions.write_csv(dict_list=self.bank_relations, filename=f'bank_relations{level}')



    def gen_persone(self):
        from Persona import Persona

        for _ in range(int(10_000*self.div)):   # 10_000 persone vengono generate nel 100% del dataset
            self.persone.append(Persona())
    
    def gen_banche(self):
        from Banca import Banca

        bank_names = []
        for _ in range(int(160*self.div)): # 160 banche vengono generate nel 100% del dataset
            new_bank = Banca()
            if new_bank.name not in bank_names:
                self.banche.append(new_bank)
                bank_names.append(new_bank.name)
    
    def gen_relazioni_banche(self):
        import random

        for banca in self.banche:
            banca2 = random.choice(self.banche)
            if banca is banca2 or banca.name in banca2.relations: 
                continue
            banca.add_relation(banca2.name)
            banca2.add_relation(banca.name)

        set_relations = set()
        list_relations = []
        for banca in self.banche:
            for banca2_name in banca.relations:
                set_relations.add(tuple(sorted([banca.name,banca2_name])))

        for relation in set_relations:
            diz = dict()
            diz['bank1'] = relation[0]
            diz['bank2'] = relation[1]
            list_relations.append(diz)
        
        self.bank_relations = list_relations



    # Vengono generate casualmente delle carte da dare alle persona già generate
    def gen_carte(self):
        import random
        from Carta import Carta

        for persona in self.persone:
            num_carte = random.randint(1,5) # numero di carte che quella persona avrà
            for _ in range(num_carte):
                c = Carta(owner=persona.cf, bank=(random.choice(self.banche)).name)
                self.carte.append(c)
    
    def gen_atms(self):
        from ATM import ATM

        for _ in range(int(320*self.div)):  # 320 ATM nel 100% del DS
            self.atms.append(ATM())
    
    def gen_transazioni(self):
        import random
        from Transazione import Transazione
        
        for _ in range(int(51200*self.div)): # 51_200 TRANSAZIONI nel 100% del DS
            carta1 = random.choice(self.carte)
            carta2 = random.choice(self.carte)
            if carta1 is carta2:
                continue
            ref_atm = random.choice(self.atms)
            t = Transazione(sender = carta1.code, receiver = carta2.code, atm = ref_atm.address)
            self.transazioni.append(t)
        


    
    