# CLASSE PERSONA 

class Persona():

    def __init__(self, cards: list = None):
        import random
        from codicefiscale import codicefiscale
        from faker import Faker

        fk = Faker('it_IT')
        self.sex = random.choice(['M','F'])
        self.name = fk.first_name_male() if self.sex == 'M' else fk.first_name_female()
        self.name = (self.name.encode('ascii','ignore')).decode('ascii')  # passaggio per evitare le lettere accentate
        self.surname = fk.last_name()
        self.dob = fk.date_of_birth()
        self.address = addr = fk.address().replace('\n',' ')[:-5]
        self.city = addr.split()[-1]
        self.phone_number = fk.phone_number()
        try:
            self.cf = codicefiscale.encode(
                name = self.name,
                surname = self.surname,
                sex = self.sex,
                birthdate = self.dob.strftime('%d/%m/%Y'),
                birthplace = self.city
            )
        except:
            self.cf = codicefiscale.encode(
                name = self.name,
                surname = self.surname,
                sex = self.sex,
                birthdate = self.dob.strftime('%d/%m/%Y'),
                birthplace = "Messina"
            )
        
        self.cards = cards if cards is not None else list()

    def has_card(self,id_card: str) -> bool:
        return id_card in self.cards

    def add_card(self,id_card: str):
        self.cards.append(id_card)
    

    def dictify(self):
        data={}

        data['name'] = self.name
        data['surname'] = self.surname
        data['sex'] = self.sex
        data['dob'] = self.dob
        data['address'] = self.address
        data['city'] = self.city
        data['phone_number'] = self.phone_number
        data['cf'] = self.cf
        return data
