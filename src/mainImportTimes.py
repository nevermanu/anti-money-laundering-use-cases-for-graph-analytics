import functions
from Neo4jController import Neo4jController 
from MongoController import MongoController
import time
import xlsxwriter

workbook = xlsxwriter.Workbook("import_times.xlsx")
worksheet = workbook.add_worksheet()

tempo = lambda: int(round(time.time() * 1000))

row = 1
col = 1

worksheet.write(0, col, "Neo4j")
worksheet.write(0, col+1, "MongoDB")

for ds in range(1,5): # DS 1, 2, 3, 4

    worksheet.write(ds, 0, f"DS {ds*25}%")

    mongo = MongoController(ds)
    mongo.reset()
    mongo1 = tempo()
    mongo.load_data()
    mongo2 = tempo()
    worksheet.write(row, col, mongo2-mongo1)
    col = 2

    neo = Neo4jController(ds)
    neo.reset()
    neo1 = tempo()
    neo.load_data()
    neo2 = tempo()
    worksheet.write(row, col, neo2-neo1)
    col = 1

    row += 1

workbook.close()


