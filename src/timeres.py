#VALUTA SINGOLARMENTE LE PERFORMANCE DI OGNI QUERY

from Neo4jController import Neo4jController 
import xlsxwriter
import time
import winsound

ds = 4 #
workbook = xlsxwriter.Workbook()

neo = Neo4jController(ds)
neo.reset()
neo.load_data()
workbook = xlsxwriter.Workbook(f'neo_quinta_query{ds}.xlsx') ####
worksheet = workbook.add_worksheet()

tempo = lambda: int(round(time.time() * 1000))

col=0
row=1

worksheet.write(0, col, "Neo4j")
for _ in range(31):
    neo1 = tempo()
    neo.query5()
    neo2 = tempo()
    worksheet.write(row, col, neo2-neo1)
    row+=1

print("fine query neo")
workbook.close()
winsound.MessageBeep()