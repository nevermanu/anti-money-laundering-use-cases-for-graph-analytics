# CLASSE BANCA
class Banca:
    def __init__(self, relations: list = None) -> None:
        from faker import Faker

        fk = Faker()
        self.name = fk.last_name()
        fk = Faker('it_IT')
        self.manager = f'{fk.first_name()} {fk.last_name()}'
        self.relations = relations if relations is not None else list()

    def add_relation(self, bank_name: str):
        self.relations.append(bank_name)

    def dictify(self):
        data={}
        data['name'] = self.name
        data['manager'] = self.manager
        return data