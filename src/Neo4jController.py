# NEO4J CLASS
class Neo4jController:

    def __init__(self, level: int) -> None:
        from neo4j import GraphDatabase

        uri = "neo4j://localhost:7687"
        user = "neo4j"
        psw = "123"
        
        self.level = level
        self.driver = GraphDatabase.driver(uri, auth=(user,psw))
        self.session = self.driver.session()

    def load_persone(self):
        
        session = self.session

        try:
            session.run("CREATE CONSTRAINT ON (p:Persona) ASSERT p.cf IS UNIQUE")
        except:
            pass

        session.run(f"""USING PERIODIC COMMIT 1024
                    LOAD CSV WITH HEADERS FROM "file:///persone{self.level}.csv"
                    AS line FIELDTERMINATOR','
                    MERGE (p:Persona{{cf:line.cf, name:line.name, surname: line.surname, sex: line.sex, dob: date(line.dob), address: line.address, city: line.city, phone_number: line.phone_number}})
        """)

    def load_carte(self):

        session = self.session

        try:
            session.run("CREATE CONSTRAINT ON (c:Carta) ASSERT c.code IS UNIQUE")
        except:
            pass
        
        session.run(f"""USING PERIODIC COMMIT 1024
                    LOAD CSV WITH HEADERS FROM "file:///carte{self.level}.csv"
                    AS line FIELDTERMINATOR','
                    MERGE (c:Carta{{code: line.code, owner: line.owner, bank: line.bank, cash: toInteger(line.cash)}})
        """)

    def load_transazioni(self):

        session = self.session
        
        session.run(f"""USING PERIODIC COMMIT 1024
                    LOAD CSV WITH HEADERS FROM "file:///transazioni{self.level}.csv"
                    AS line FIELDTERMINATOR','
                    MERGE (t:Transazione{{sender: line.sender, receiver: line.receiver, atm: line.atm, amount: toInteger(line.amount), timestamp: line.timestamp}})
        """)

    def load_atm(self):
        session = self.session

        try:
            session.run("CREATE CONSTRAINT ON (a:ATM) ASSERT a.address IS UNIQUE")
        except:
            pass
        
        session.run(f"""USING PERIODIC COMMIT 1024
                    LOAD CSV WITH HEADERS FROM "file:///atms{self.level}.csv"
                    AS line FIELDTERMINATOR','
                    MERGE (a:ATM{{address: line.address}})
        """)

    def load_banche(self):

        session = self.session

        try:
            session.run("CREATE CONSTRAINT ON (b:Banca) ASSERT b.name IS UNIQUE")
        except:
            pass

        session.run(f"""USING PERIODIC COMMIT 1024
                    LOAD CSV WITH HEADERS FROM "file:///banche{self.level}.csv"
                    AS line FIELDTERMINATOR','
                    MERGE (b:Banca{{name: line.name, manager: line.manager}})
        """)

    def create_edges(self):

        session = self.session

        # LOAD DELLE RELAZIONI TRA LE BANCHE->BANCHE
        session.run(f"""USING PERIODIC COMMIT 1024
                    LOAD CSV WITH HEADERS FROM "file:///bank_relations{self.level}.csv"
                    AS line FIELDTERMINATOR','
                    MATCH (b1:Banca{{name: line.bank1}})
                    MATCH (b2:Banca{{name: line.bank2}})
                    MERGE (b1)-[:Relation]->(b2)
                    MERGE (b2)-[:Relation]->(b1)
        """)

        # CREAZIONE DELLE RELAZIONI TRA BANCHE->CARTE E PERSONE->CARTE
        cards = session.run("MATCH (c:Carta) return c")
        card_objects = []
        for card in cards:
            card_objects.append(card)
        
        for card in card_objects:
            session.run("""MATCH (p:Persona{cf: $owner})
                            MATCH (c:Carta{code: $code})
                            MATCH (b:Banca{name: $bank})
                            MERGE (p)-[:Owns]->(c)
                            MERGE (b)-[:Provides]->(c)
            """, owner = card['c'].get('owner'), code = card['c'].get('code'), bank = card['c'].get('bank'))

        # CREAZIONE DELLE RELAZIONI TRA CARTE->TRANSAZIONI(SENDER), TRANSAZIONI->CARTE(RECEIVER), TRANSAZIONI->ATM
        transactions = session.run("MATCH (t:Transazione) return t")

        transaction_objects = []
        for transaction in transactions:
            transaction_objects.append(transaction)
        
        for transaction in transaction_objects:
            session.run("""MATCH (t:Transazione)
                            WHERE id(t) = toInteger($idt)
                            MATCH (c_sender:Carta{code: $sender})
                            MATCH (c_receiver:Carta{code: $receiver})
                            MATCH (a:ATM{address: $atm})
                            MERGE (c_sender)-[:Sends]->(t)
                            MERGE (c_receiver)<-[:Receives]-(t)
                            MERGE (t)-[:By]->(a)
            """, idt = transaction['t'].id, sender =  transaction['t'].get('sender'), receiver = transaction['t'].get('receiver'), atm = transaction['t'].get('atm'))
   
   
    def query1(self):
        # TUTTE LE TRANSAZIONI CON AMOUNT > 1000
        session = self.session

        session.run("MATCH (t:Transazione) WHERE t.amount > 1000 RETURN t")

    def query2(self):
        # TROVA LE PERSONE CHE POSSEGGONO UNA CARTA DI UNA DETERMINATA BANCA
        session = self.session

        bank_name = ['Jarvis', 'Mitchell', 'Daniels', 'Clark'][self.level-1]
        session.run('MATCH (p:Persona)-[:Owns]->(:Carta)<-[:Provides]-(:Banca{name: $bank}) RETURN p', bank = bank_name)
    
    def query3(self):
        # TROVA LE PERSONE CHE POSSEGGONO UNA CARTA DELLE BANCHE CON CUI HA RAPPORTI UNA DETERMINATA BANCA
        session = self.session

        bank_name = ['Jarvis', 'Mitchell', 'Daniels', 'Clark'][self.level-1]
        session.run('MATCH (p:Persona)-[:Owns]->(:Carta)<-[:Provides]-(:Banca)-[:Relation]-(:Banca {name: $bank}) RETURN p', bank = bank_name)

    def query4(self):
         # TROVA GLI ATM IN CUI SONO AVVENUTE DELLE TRANSAZIONI SUPERIORI A 1000 
         # CON MITTENTE UNA CARTA DI UNA DETERMINATA BANCA
         session = self.session

         bank_name = ['Jarvis', 'Mitchell', 'Daniels', 'Clark'][self.level-1]
         session.run('MATCH (:Banca {name: $bank})-[:Provides]->(:Carta)-[:Receives]-(t:Transazione)-[:By]-(a:ATM) WHERE t.amount > 1000 return a', bank = bank_name)
    
    
    def query5(self):
        # TROVA LE PERSONE CHE HANNO CARTE CON PIU' DI 800.000 DI CASH CHE HANNO ESEGUITO TRANSAZIONI IN USCITA (SENDER)
        # DI PIU' DI 2000 DI AMOUNT CON ALTRE CARTE CON MENO DI 100.000 DI CASH
        session = self.session

        session.run('MATCH (p:Persona)-[:Owns]->(c1:Carta)-[:Sends]->(t:Transazione)-[:Receives]->(c2:Carta) WHERE c1.cash > 800000 AND c2.cash < 100000 AND t.amount > 2000 RETURN DISTINCT p')

    def load_data(self):

        self.load_persone()
        self.load_atm()
        self.load_banche()
        self.load_carte()
        self.load_transazioni()
        self.create_edges()

    
    def reset(self):

        session = self.session

        session.run("MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n, r")
        try:
            session.run("DROP CONSTRAINT ON (b:Banca) ASSERT b.name IS UNIQUE")
            session.run("DROP CONSTRAINT ON (a:ATM) ASSERT a.address IS UNIQUE")
            session.run("DROP CONSTRAINT ON (c:Carta) ASSERT c.code IS UNIQUE")
            session.run("DROP CONSTRAINT ON (p:Persona) ASSERT p.cf IS UNIQUE")
        except:
            pass